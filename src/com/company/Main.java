package com.company;

public class Main {

    public static void main(String[] args) {

        int switchValue = 4;
        //good to use when testing the same variable and you want to test different values for that variable
        switch(switchValue) {
            case 1:
                System.out.println("Value was 1");
                break;
            case 2:
                System.out.println("Value was 2");
                break;
            case 3: case 4: case 5:
                System.out.println("Value was 3, 4, or 5");
                System.out.println("The value was " + switchValue);
                break;
            default:
                System.out.println("was not 1 or 2");
                break;
        }
        /*Create a new switch statement using char instead of int
        * create a new char variable
        * create a switch statement testing for A, B, C, D, or E
        * display a message if any of these are found and then break
        * add a default which display message not found*/

        char switchValue2 =  'F';
        switch(switchValue2) {
            case 'A':
                System.out.println("char is equal to A");
                break;
            case 'B':
                System.out.println("char is equal to B");
                break;
            case 'C':
                System.out.println("char is equal to C");
                break;
            case 'D':
                System.out.println("char is equal to D");
                break;
            case 'E':
                System.out.println("char is equal to E");
                break;
            default:
                System.out.println("not found");
                break;
        }

    }
}
